class CommentsController < ApplicationController
  before_filter :require_login, :only => [:create, :destroy]

  def create
    @article = Article.find_by_id(params[:comment][:article_id])
    @comment = @article.comments.create(:content => params[:comment][:comment], :user_id => params[:comment][:user_id])
    @comments = @article.comments
    respond_to do |format|
      if @comment.save
        format.html { redirect_to(article_path(params[:comment][:article_id]), :notice => 'Comment was successfully created.') }
        format.js
      end
    end
  end

  def destroy
    @comment = Comment.find_by_id(params[:id])
    if @comment.destroy
      flash[:notice] = "Your comment has been deleted"
      redirect_to article_path(params[:article_id])
    else
      flash[:error] = "Your comment is failed to create"
      redirect_to article_path(params[:article_id])
    end
  end

end
