class SessionsController < ApplicationController

  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      session[:user_id] = user.id
      if user.is_admin?
        redirect_to admin_articles_path, :notice => "Admin logged in!"
      else
        redirect_to articles_path, :notice => "User Logged in!"
      end
      # redirect_to articles_path, :notice => "Logged in!"
    else
      flash.now.alert = "Invalid email or password"  
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to log_in_path, :notice => "Logged out!"
  end

end
