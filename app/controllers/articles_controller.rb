class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  
  def show
    @article = Article.find(params[:id])
    @comment = Comment.new
    @comments = @article.comments
  end

  def new
    @article = Article.new  
  end

  def create
    @article= Article.new(params[:article].merge({:user_id => current_user.id, :rating => 1}))
    if @article.save
      flash[:notice] = "Article successfully saved"
      redirect_to :action => :index
    else
      flash.now[:notice] = "Article failed to saved"
      render :action => :new
    end
  end
  
  def edit
    if Article.find(params[:id]).user_id == current_user.id
      @article = Article.find(params[:id])
    else
      redirect_to articles_path, :notice => 'you cant edit'
    end
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      redirect_to articles_path, :notice => 'Article was successfully updated.'
    else
      flash[:error] = 'Article was failed to update.'
      render :action => 'edit'
    end
  end

  def index
    @articles = Article.all
    @user = User.all
    @article_hundred_char = Article.hundred_char
  end

  def destroy
    if Article.find(params[:id]).user_id == current_user.id
      Article.destroy(params[:id])
      redirect_to articles_path, :notice => 'Article was successfully deleted'
    else
      redirect_to articles_path, :notice => 'you cant delete'
    end
  end

end
