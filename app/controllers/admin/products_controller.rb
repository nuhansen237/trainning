class Admin::ProductsController < Admin::ApplicationController
  before_filter :require_admin_login

  def index
    @products = Product.all
    @product = Product.price_more_than_1000
    @red = Product.containing_red
  end

  def new
    @product = Product.new
    @categories = Category.all.map{|x| [x.name, x.id]}
  end

  def create
    @product = User.first.products.new(params[:product])
    @categories = Category.all.map{|x| [x.name, x.id]}
    if @product.save
      flash[:notice] = "Your product has been created"
      redirect_to admin_products_path
    else
      flash[:error] = "Your product failed to create"
      render :new
    end
  end

  def show
    @product = Product.find(params[:id])
  end
  
end
