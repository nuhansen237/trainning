class Admin::ArticlesController < Admin::ApplicationController
	before_filter :require_admin_login

  def index
    @articles = Article.all
    @user = User.all
  end

	def show
    @article = Article.find(params[:id])
    @comment = Comment.new
    @comments = @article.comments
  end

  def new
    @article = Article.new  
  end

  def create
    @article= Article.new(params[:article].merge({:user_id => current_user.id, :rating => 1}))
    if @article.save
      flash[:notice] = "Article successfully saved"
      redirect_to admin_articles_path
    else
      flash.now[:notice] = "Article failed to saved"
      render :action => :new
    end
  end
  
  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      redirect_to admin_articles_path, :notice => 'Article was successfully updated.'
    else
      flash[:error] = 'Article was failed to update.'
      render :action => 'edit'
    end
  end

  def destroy
    if Article.destroy(params[:id])
      redirect_to admin_articles_path, :notice => 'Article was successfully deleted'
    else
      redirect_to admin_articles_path, :notice => 'you cant delete'
    end
  end

end
