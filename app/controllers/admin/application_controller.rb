class Admin::ApplicationController < ApplicationController
  protect_from_forgery
  helper_method :current_user

protected
  def require_admin_login
     if !current_user || !current_user.is_admin?
       flash[:error] = "Only admins are permitted"
       redirect_to log_in_path
    else
       return current_user
    end
  end
end