class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if verify_recaptcha
      if @user.save
        UserMailer.registration_confirmation(@user).deliver
        redirect_to log_in_url, :notice => "login!"
      else
        render "new"
      end
      else
    flash[:error] = "There was an error with the recaptcha code below.
                     Please re-enter the code and click submit."
    render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end 

end
