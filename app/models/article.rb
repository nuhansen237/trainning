class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments

  scope :rating_is_or_above, lambda {|rating| where("rating = ?", rating)}
  
  validates :title,  :uniqueness => true, :presence => true                  
  
  validate :valid_title
  def valid_title
    self.errors[:title] << "can't be filled with by 'nil', 'empty' , and 'blank'" if (title == "nil" || title == "blank" || title == "empty")
  end

  def self.hundred_char
    self.where("LENGTH(body) > 100")
  end
  
  def search_article 
     Article.where("title ilike ?", "%my country%")
  end  
  
end
