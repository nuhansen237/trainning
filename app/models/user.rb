class User < ActiveRecord::Base
  attr_accessor :password
  attr_accessible :is_admin, :first_name, :last_name, :email, :password, :password_confirmation, :age, :address
  before_save :encrypt_password
  
  has_many :articles, :dependent => :destroy
  has_many :products, :dependent => :destroy
  belongs_to :country

  has_many :shared_articles, :class_name => "Article", 
                             :foreign_key => "user_id",
                             :conditions => "title like '%my country%'"
  validates :password, :presence => {:on => :create},
                       :confirmation => true
  validates :first_name, :presence => true,
                         :length => {:minimum => 1, :maximum => 20},
                         :format => {:with => /[a-zA-Z\s]+$/},
                         :uniqueness => true
  validates :last_name, :presence => true,
                        :length => {:minimum => 1, :maximum => 20},
                        :format => {:with => /[a-zA-Z\s]+$/},
                        :uniqueness => true
  validates :email, :presence => true,
            :uniqueness => true

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end
  
  def full_address
    "#{self.address} #{self.country.name}"
  end
end 
