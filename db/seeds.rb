# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([{
  :first_name => "user satu",
  :last_name => "satu",
  :email => "user1@yahoo.com",
  :username => "user1",
  :address => "jl. bandung1",
  :age => 20, 
  :birthday => "1990-01-01" 
  },{
  :first_name => "user dua",
  :last_name => "dua",
  :email => "user2@yahoo.com",
  :username => "user2",
  :address => "jl. bandung2",
  :age => 21, 
  :birthday => "1990-01-02" 
  },{
  :first_name => "user tiga",
  :last_name => "tiga",
  :email => "user3@yahoo.com",
  :username => "user3",
  :address => "jl. bandung3",
  :age => 22, 
  :birthday => "1990-01-03" 
  },{
  :first_name => "user empat",
  :last_name => "empat",
  :email => "user4@yahoo.com",
  :username => "user4",
  :address => "jl. bandung4",
  :age => 23, 
  :birthday => "1990-01-04"
  },{
  :first_name => "user lima",
  :last_name => "lima",
  :email => "user5@yahoo.com",
  :username => "user5",
  :address => "jl. bandung5",
  :age => 24, 
  :birthday => "1990-01-05"
  }])
  
article = Article.create([{
  :title => "Article 1", 
  :body => "Content Article 1" 
  },{
  :title => "Article 2", 
  :body => "Content Article 2" 
  },{
  :title => "Article 3", 
  :body => "Content Article 3" 
  },{
  :title => "Article 4", 
  :body => "Content Article 4" 
  },{
  :title => "Article 5", 
  :body => "Content Article 5" 
  }])
  
comment = Comment.create([{
  :content => "Content 1",
  :article_id => 1
  },{
  :content => "Content 2",
  :article_id => 2
  },{
  :content => "Content 3",
  :article_id => 3
  },{
  :content => "Content 4",
  :article_id => 4
  },{
  :content => "Content 5",
  :article_id => 5
  }])
  
country = Country.create([{
  :code => "id", :name => "indonesia"
  },{
  :code => "usa", :name => "Amerika"
  },{
  :code => "frc", :name => "Francis"
  }])
  
category = Category.create([{
  :name => "category 1"
  },{
  :name => "category 2"
  },{
  :name => "category 3"
  },{
  :name => "category 4"
  },{
  :name => "category 5"
  }])
  
